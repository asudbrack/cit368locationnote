﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LocationNote
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["aws"].ConnectionString;

		int userID = 0;
		int currentTab = 0;
		int badLoginCounter = 0;

		int selectedNoteID = 0;
		string selectedNote;
		string selectedLocation;
		
		public MainWindow()
		{
			InitializeComponent();
			gridLogin.Visibility = Visibility.Visible;
			gridRegister.Visibility = Visibility.Hidden;
			gridNotes.Visibility = Visibility.Hidden;
			tabSidebar.Visibility = Visibility.Hidden;
			gridEditNote.Visibility = Visibility.Hidden;
			gridEditProfile.Visibility = Visibility.Hidden;
			logOutButton.Visibility = Visibility.Hidden;
		}

		private void switchToRegister(object sender, RoutedEventArgs e)
		{
			gridRegister.Visibility = Visibility.Visible;
			gridLogin.Visibility = Visibility.Hidden;
			gridNotes.Visibility = Visibility.Hidden;
		}

		private void switchToLogin(object sender, RoutedEventArgs e)
		{
			gridLogin.Visibility = Visibility.Visible;
			gridRegister.Visibility = Visibility.Hidden;
			gridNotes.Visibility = Visibility.Hidden;
		}

		private void switchToNotes(object sender, RoutedEventArgs e)
		{
			gridNotes.Visibility = Visibility.Visible;
			stackAddNote.Visibility = Visibility.Visible;
			gridLogin.Visibility = Visibility.Hidden;
			gridRegister.Visibility = Visibility.Hidden;
			gridEditNote.Visibility = Visibility.Hidden;
			gridEditProfile.Visibility = Visibility.Hidden;

			gridEditName.Visibility = Visibility.Hidden;
			gridEditEmail.Visibility = Visibility.Hidden;
			gridEditPassword.Visibility = Visibility.Hidden;
			
			currentTab = 0;
			FillDataGrid(currentTab);
		}

		private void switchToNotes()
		{
			gridNotes.Visibility = Visibility.Visible;
			stackAddNote.Visibility = Visibility.Visible;
			gridLogin.Visibility = Visibility.Hidden;
			gridRegister.Visibility = Visibility.Hidden;
			gridEditNote.Visibility = Visibility.Hidden;
			gridEditProfile.Visibility = Visibility.Hidden;

			gridEditName.Visibility = Visibility.Hidden;
			gridEditEmail.Visibility = Visibility.Hidden;
			gridEditPassword.Visibility = Visibility.Hidden;

			currentTab = 0;
			FillDataGrid(currentTab);
		}

		private void switchToFinished(object sender, RoutedEventArgs e)
		{
			gridNotes.Visibility = Visibility.Visible;
			gridLogin.Visibility = Visibility.Hidden;
			gridRegister.Visibility = Visibility.Hidden;
			gridEditNote.Visibility = Visibility.Hidden;
			gridEditProfile.Visibility = Visibility.Hidden;
			stackAddNote.Visibility = Visibility.Hidden;

			gridEditName.Visibility = Visibility.Hidden;
			gridEditEmail.Visibility = Visibility.Hidden;
			gridEditPassword.Visibility = Visibility.Hidden;

			currentTab = 1;
			FillDataGrid(currentTab);
		}

		private void switchToProfile(object sender, RoutedEventArgs e)
		{
			gridEditProfile.Visibility = Visibility.Visible;
			gridNotes.Visibility = Visibility.Hidden;
			gridLogin.Visibility = Visibility.Hidden;
			gridRegister.Visibility = Visibility.Hidden;
			gridEditNote.Visibility = Visibility.Hidden;

			gridEditName.Visibility = Visibility.Hidden;
			gridEditEmail.Visibility = Visibility.Hidden;
			gridEditPassword.Visibility = Visibility.Hidden;
			try
			{
				using (MySqlConnection connection = new MySqlConnection(connectionString))
				{
					connection.Open();
					var command = new MySqlCommand("SELECT Name FROM User WHERE UserID = @userID", connection);
					command.Parameters.AddWithValue("@userID", userID);
					command.Prepare();
					txtProfileWelcome.Text = "Hello, "+Convert.ToString(command.ExecuteScalar());

					connection.Close();
				}
			}
			catch (MySqlException ex)
			{
				MessageBox.Show("Database Error " + ex.Number, "Error");
			}
		}

		private void btnLogOut(object sender, RoutedEventArgs e)
		{
			gridLogin.Visibility = Visibility.Visible;
			gridNotes.Visibility = Visibility.Hidden;
			stackAddNote.Visibility = Visibility.Hidden;
			gridRegister.Visibility = Visibility.Hidden;
			gridEditNote.Visibility = Visibility.Hidden;
			gridEditProfile.Visibility = Visibility.Hidden;
			tabSidebar.Visibility = Visibility.Hidden;

			gridEditName.Visibility = Visibility.Hidden;
			gridEditEmail.Visibility = Visibility.Hidden;
			gridEditPassword.Visibility = Visibility.Hidden;

			logOutButton.Visibility = Visibility.Hidden;

			txtLoginEmail.Clear();
			txtLoginPassword.Clear();

			userID = 0;
		}

		private void btnCreateAccount(object sender, RoutedEventArgs e)
		{
			string registerName = txtName.Text;
			string registerEmail = txtEmail.Text;
			string registerPassword = txtPassword.Password;
			string registerConfirmPassword = txtConfirmPassword.Password;
			string[] registerInfo = new string[] { registerName, registerEmail, registerPassword, registerConfirmPassword };

			if (InputValidation(registerInfo))
			{
				if(EmailValidation(registerEmail))
				{
					if (registerPassword.Equals(registerConfirmPassword))
					{
						try
						{
							using (MySqlConnection connection = new MySqlConnection(connectionString))
							{
								connection.Open();
								var command = new MySqlCommand("SELECT COUNT(Email) FROM User WHERE Email = @email;", connection);
								command.Parameters.AddWithValue("@email", registerEmail);
								command.Prepare();
								int result = Convert.ToInt32(command.ExecuteScalar());

								if (result > 0)
								{
									MessageBox.Show("Email is already taken!", "Error");

								}
								else
								{
									createAccount(registerName, registerEmail, registerPassword);
								}

								connection.Close();
							}
						}
						catch (MySqlException ex)
						{
							MessageBox.Show("Database Error " + ex.Number, "Error");
						}
					}
					else
					{
						MessageBox.Show("Passwords do not match!", "Error");
					}
				}
				else
				{
					MessageBox.Show("Email is not in the correct format", "Error");
				}
			}
			else
			{
				MessageBox.Show("One or more fields are empty, too long, or include special characters.", "Error");
			}

		}

		private void createAccount(string name, string email, string password)
		{
			string hashedPassword = HashPassword(password);
			try
			{
				using (MySqlConnection connection = new MySqlConnection(connectionString))
				{
					connection.Open();
					var command = new MySqlCommand("INSERT INTO User(Name, Email, Password) VALUES(@name, @email, @password)", connection);
					command.Parameters.AddWithValue("@name", name);
					command.Parameters.AddWithValue("@email", email);
					command.Parameters.AddWithValue("@password", hashedPassword);
					command.Prepare();
					int result = command.ExecuteNonQuery();

					if (result > 0)
					{
						MessageBox.Show("Hello, " + name + "! You can now Login.", "Success");

						txtLoginEmail.Clear();
						txtLoginPassword.Clear();
						txtName.Clear();
						txtEmail.Clear();
						txtPassword.Clear();
						txtConfirmPassword.Clear();
					}

					connection.Close();

				}
			}
			catch (MySqlException ex)
			{
				MessageBox.Show("Database Error " + ex.Number, "Error");
			}
		}

		private void btnLogin(object sender, RoutedEventArgs e)
		{
			string loginEmail = txtLoginEmail.Text;
			string loginPassword = txtLoginPassword.Password;
			string[] loginInfo = new string[] { loginEmail, loginPassword };

			
			if (InputValidation(loginInfo))
			{
				startLogin(loginEmail, loginPassword);
			}
			else
			{
				MessageBox.Show("One or more fields are empty, too long, or include special characters.", "Error");
			}

		}

		private void startLogin(string email, string password)
		{
			userID = 0;
			try
			{
				using (MySqlConnection connection = new MySqlConnection(connectionString))
				{
					connection.Open();
					var command = new MySqlCommand("SELECT userid, password FROM User WHERE email = @email", connection);
					command.Parameters.AddWithValue("@email", email);
					command.Prepare();

					MySqlDataReader reader = command.ExecuteReader();

					if (reader.Read())
					{
						string hashedPassword = reader[1].ToString();
						bool isSamePassword = VerifyPassword(password, hashedPassword);

						if (isSamePassword)
						{
							userID = Convert.ToInt32(reader[0].ToString());
							tabSidebar.Visibility = Visibility.Visible;
							switchToNotes();
							logOutButton.Visibility = Visibility.Visible;

						}
						else
						{
							MessageBox.Show("Incorrect Login", "Error");
							badLoginCounter++;
						}
					}
					else
					{
						MessageBox.Show("Incorrect Login", "Error");
						badLoginCounter++;
					}

					connection.Close();
				}
			}
			catch (MySqlException ex)
			{
				MessageBox.Show("Database Error " + ex.Number, "Error");
			}

			if (badLoginCounter >= 5)
			{
				buttonLogin.IsEnabled = false;
				MessageBox.Show("Too Many Login Attempts", "Message");
			}
			
		}

		private void FillDataGrid(int currentTab)
		{
			try
			{
				using (MySqlConnection connection = new MySqlConnection(connectionString))
				{
					string noteQuery = null;

					switch (currentTab)
					{
						case 0:
							noteQuery = "SELECT Finished, Note, Location, NoteID FROM Note WHERE UserID = @userID AND finished = 0;";
							break;
						case 1:
							noteQuery = "SELECT Finished, Note, Location, NoteID FROM Note WHERE UserID = @userID AND finished = 1;";
							break;
					}

					connection.Open();
					var command = new MySqlCommand(noteQuery, connection);
					command.Parameters.AddWithValue("@userID", userID);
					command.Prepare();


					MySqlDataAdapter sqlAdapter = new MySqlDataAdapter(command);
					DataTable dt = new DataTable("NoteData");

					sqlAdapter.Fill(dt);
					datagridNotes.ItemsSource = dt.DefaultView;
					connection.Close();
				}
			}
			catch (MySqlException ex)
			{
				MessageBox.Show("Database Error " + ex.Number, "Error");
			}
		}


		private void btnAddNote(object sender, RoutedEventArgs e)
		{

			string note = txtAddNote.Text;
			string location = txtAddLocation.Text;
			string[] noteInfo = new string[] { note, location };

			try
			{
				using (MySqlConnection connection = new MySqlConnection(connectionString))
				{
					if (InputValidation(noteInfo))
					{
						connection.Open();
						var command = new MySqlCommand("INSERT INTO Note(Note, Location, Finished, UserID) VALUES(@note, @location, @finished, @userID)", connection);
						command.Parameters.AddWithValue("@note", note);
						command.Parameters.AddWithValue("@location", location);
						command.Parameters.AddWithValue("@finished", 0);
						command.Parameters.AddWithValue("@userID", userID);
						command.Prepare();

						int result = command.ExecuteNonQuery();

						if (result > 0)
						{
							FillDataGrid(currentTab);
							txtAddLocation.Clear();
							txtAddNote.Clear();
						}
					}
					else
					{
						MessageBox.Show("One or more fields are empty, too long, or include special characters.", "Error");
					}

					connection.Close();
				}
			}
			catch (MySqlException ex)
			{
				MessageBox.Show("Database Error " + ex.Number, "Error");
			}
		}

		private void btnEditNote(object sender, RoutedEventArgs e)
		{
			gridEditNote.Visibility = Visibility.Visible;
			gridNotes.Visibility = Visibility.Hidden;

			txtEditNote.Text = selectedNote;
			txtEditLocation.Text = selectedLocation;

		}

		private void btnUpdateNote(object sender, RoutedEventArgs e)
		{
			string updatedNote = txtEditNote.Text;
			string updatedLocation = txtEditLocation.Text;
			string[] updatedNoteInfo = new string[] { updatedNote, updatedLocation };

			if(InputValidation(updatedNoteInfo))
			{
				try
				{
					using (MySqlConnection connection = new MySqlConnection(connectionString))
					{
						connection.Open();
						var command = new MySqlCommand("UPDATE Note SET Note = @updatedNote, Location = @updatedLocation WHERE NoteID = @selectedNoteID", connection);
						command.Parameters.AddWithValue("@updatedNote", updatedNote);
						command.Parameters.AddWithValue("@updatedLocation", updatedLocation);
						command.Parameters.AddWithValue("@selectedNoteID", selectedNoteID);
						command.Prepare();

						int result = command.ExecuteNonQuery();

						if (result > 0)
						{
							FillDataGrid(currentTab);
							txtEditNote.Clear();
							txtEditLocation.Clear();
							gridEditNote.Visibility = Visibility.Hidden;
							gridNotes.Visibility = Visibility.Visible;
						}

						connection.Close();
					}
				}
				catch (MySqlException ex)
				{
					MessageBox.Show("Database Error " + ex.Number, "Error");
				}
			}
			else
			{
				MessageBox.Show("One or more fields are empty, too long, or include special characters.", "Error");
			}

			

		}

		private void btnUpdateCancelNote(object sender, RoutedEventArgs e)
		{
			FillDataGrid(currentTab);
			gridEditNote.Visibility = Visibility.Hidden;
			gridNotes.Visibility = Visibility.Visible;
			txtEditNote.Clear();
			txtEditLocation.Clear();
		}

		private void btnDeleteNote(object sender, RoutedEventArgs e)
		{
			try
			{
				using (MySqlConnection connection = new MySqlConnection(connectionString))
				{
					connection.Open();
					var command = new MySqlCommand("DELETE FROM Note WHERE NoteID = @selectedNoteID", connection);
					command.Parameters.AddWithValue("@selectedNoteID", selectedNoteID);
					command.Prepare();

					int result = command.ExecuteNonQuery();

					if (result > 0)
					{
						FillDataGrid(currentTab);
					}

					connection.Close();
				}
			}
			catch (MySqlException ex)
			{
				MessageBox.Show("Database Error " + ex.Number, "Error");
			}

		}

		private void btnFinishNote(object sender, RoutedEventArgs e)
		{
			try
			{
				using (MySqlConnection connection = new MySqlConnection(connectionString))
				{
					string finishNoteQuery = null;

					if (currentTab == 0)
					{
						finishNoteQuery = "UPDATE Note SET Finished = 1 WHERE NoteID = @selectedNoteID;";
					}
					else if (currentTab == 1)
					{
						finishNoteQuery = "UPDATE Note SET Finished = 0 WHERE NoteID = @selectedNoteID;";
					}

					connection.Open();
					var command = new MySqlCommand(finishNoteQuery, connection);
					command.Parameters.AddWithValue("@selectedNoteID", selectedNoteID);
					command.Prepare();

					int result = command.ExecuteNonQuery();

					if (result > 0)
					{
						FillDataGrid(currentTab);
					}

					connection.Close();
				}
			}
			catch (MySqlException ex)
			{
				MessageBox.Show("Database Error " + ex.Number, "Error");
			}
		}

		private void datagridNotes_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			var selectedItem = datagridNotes.SelectedItem as DataRowView;

			if (selectedItem != null)
			{
				selectedNote = selectedItem.Row[1].ToString();
				selectedLocation = selectedItem.Row[2].ToString();
				selectedNoteID = Convert.ToInt32(selectedItem.Row[3].ToString());
			}

		}

		private void btnEditName(object sender, RoutedEventArgs e)
		{
			gridEditName.Visibility = Visibility.Visible;
			gridEditProfile.Visibility = Visibility.Hidden;

			try
			{
				using (MySqlConnection connection = new MySqlConnection(connectionString))
				{
					connection.Open();
					var command = new MySqlCommand("SELECT Name FROM User WHERE UserID = @userID", connection);
					command.Parameters.AddWithValue("@userID", userID);
					command.Prepare();

					txtEditName.Text = Convert.ToString(command.ExecuteScalar());

					connection.Close();
				}
			}
			catch (MySqlException ex)
			{
				MessageBox.Show("Database Error " + ex.Number, "Error");
			}
		}

		private void btnUpdateName(object sender, RoutedEventArgs e)
		{
			string updatedName = txtEditName.Text;

			if(InputValidation(updatedName))
			{
				try
				{
					using (MySqlConnection connection = new MySqlConnection(connectionString))
					{
						connection.Open();
						var command = new MySqlCommand("UPDATE User SET Name = @updatedName WHERE UserID = @userID", connection);
						command.Parameters.AddWithValue("@updatedName", updatedName);
						command.Parameters.AddWithValue("@userID", userID);
						command.Prepare();

						int result = command.ExecuteNonQuery();

						if (result > 0)
						{
							FillDataGrid(currentTab);
							txtEditName.Clear();
							gridEditName.Visibility = Visibility.Hidden;
							gridEditProfile.Visibility = Visibility.Visible;
							txtProfileWelcome.Text = "Hello, " + updatedName;
							MessageBox.Show("Name changed!", "Success");
						}

						connection.Close();
					}
				}
				catch (MySqlException ex)
				{
					MessageBox.Show("Database Error " + ex.Number, "Error");
				}
			}
			else
			{
				MessageBox.Show("One or more fields are empty, too long, or include special characters.", "Error");
			}
			
		}
		private void btnUpdateCancelName(object sender, RoutedEventArgs e)
		{
			FillDataGrid(currentTab);
			gridEditName.Visibility = Visibility.Hidden;
			gridEditProfile.Visibility = Visibility.Visible;
			txtEditNote.Clear();
			
		}

		private void btnEditEmail(object sender, RoutedEventArgs e)
		{
			gridEditEmail.Visibility = Visibility.Visible;
			gridEditProfile.Visibility = Visibility.Hidden;
			try
			{
				using (MySqlConnection connection = new MySqlConnection(connectionString))
				{
					connection.Open();
					var command = new MySqlCommand("SELECT Email FROM User WHERE UserID = @userID", connection);
					command.Parameters.AddWithValue("@userID", userID);
					command.Prepare();

					txtEditEmail.Text = Convert.ToString(command.ExecuteScalar());

					connection.Close();
				}
			}
			catch (MySqlException ex)
			{
				MessageBox.Show("Database Error " + ex.Number, "Error");
			}
		}

		private void btnUpdateEmail(object sender, RoutedEventArgs e)
		{
			string updatedEmail = txtEditEmail.Text;

			if(InputValidation(updatedEmail))
			{
				if(EmailValidation(updatedEmail))
				{
					try
					{
						using (MySqlConnection connection = new MySqlConnection(connectionString))
						{
							connection.Open();
							var command = new MySqlCommand("SELECT COUNT(Email) FROM User WHERE Email = @updatedEmail;", connection);
							command.Parameters.AddWithValue("@updatedEmail", updatedEmail);
							command.Prepare();

							int queryResult = Convert.ToInt32(command.ExecuteScalar());

							if (queryResult > 0)
							{
								MessageBox.Show("Email is already taken", "Error");
							}
							else
							{
								command = new MySqlCommand("UPDATE User SET Email = @updatedEmail WHERE UserID = @userID", connection);
								command.Parameters.AddWithValue("@updatedEmail", updatedEmail);
								command.Parameters.AddWithValue("@userID", userID);
								command.Prepare();

								int postResult = command.ExecuteNonQuery();

								if (postResult > 0)
								{
									FillDataGrid(currentTab);
									txtEditEmail.Clear();
									gridEditEmail.Visibility = Visibility.Hidden;
									gridEditProfile.Visibility = Visibility.Visible;
									MessageBox.Show("Email changed!", "Success");
								}

							}

							connection.Close();
						}
					}
					catch (MySqlException ex)
					{
						MessageBox.Show("Database Error " + ex.Number, "Error");
					}
				}
				else
				{
					MessageBox.Show("Email is not in the correct format", "Error");
				}
				
			}
			else
			{
				MessageBox.Show("One or more fields are empty, too long, or include special characters.", "Error");
			}

		}
		private void btnUpdateCancelEmail(object sender, RoutedEventArgs e)
		{
			FillDataGrid(currentTab);
			gridEditEmail.Visibility = Visibility.Hidden;
			gridEditProfile.Visibility = Visibility.Visible;
			txtEditEmail.Clear();
		}

		private void btnEditPassword(object sender, RoutedEventArgs e)
		{
			gridEditPassword.Visibility = Visibility.Visible;
			gridEditProfile.Visibility = Visibility.Hidden;
		}

		private void btnUpdatePassword(object sender, RoutedEventArgs e)
		{
			string currentPassword = txtEditCurrentPassword.Password;
			string updatedPassword = txtEditPassword.Password;
			string updatedConfirmPassword = txtEditConfirmPassword.Password;
			string[] updatedPasswordInfo = new string[] { currentPassword, updatedPassword, updatedConfirmPassword };

			if(InputValidation(updatedPasswordInfo))
			{
				if(updatedPassword.Equals(updatedConfirmPassword))
				{
					try
					{
						using (MySqlConnection connection = new MySqlConnection(connectionString))
						{
							connection.Open();
							var command = new MySqlCommand("SELECT Password FROM User WHERE UserID = @userID", connection);
							command.Parameters.AddWithValue("@userID", userID);
							command.Prepare();

							string dbHashedPassword = Convert.ToString(command.ExecuteScalar());

							bool isSamePassword = VerifyPassword(currentPassword, dbHashedPassword);

							if (isSamePassword)
							{
								UpdatePassword(HashPassword(updatedPassword));
							}
							else
							{
								MessageBox.Show("Incorrect Current Password", "Error");
							}

							connection.Close();
						}
					}
					catch (MySqlException ex)
					{
						MessageBox.Show("Database Error " + ex.Number, "Error");
					}
				} 
				else
				{
					MessageBox.Show("New Passwords do not match!", "Error");
				}
			}
			else
			{
				MessageBox.Show("One or more fields are empty, too long, or include special characters.", "Error");
			}

		}

		private void UpdatePassword(string updatedPassword)
		{
			try
			{
				using (MySqlConnection connection = new MySqlConnection(connectionString))
				{
					connection.Open();
					var command = new MySqlCommand("UPDATE User SET Password = @updatedPassword WHERE UserID = @userID", connection);
					command.Parameters.AddWithValue("@updatedPassword", updatedPassword);
					command.Parameters.AddWithValue("@userID", userID);
					command.Prepare();

					int result = command.ExecuteNonQuery();

					if (result > 0)
					{
						FillDataGrid(currentTab);
						txtEditCurrentPassword.Clear();
						txtEditPassword.Clear();
						txtEditConfirmPassword.Clear();
						gridEditPassword.Visibility = Visibility.Hidden;
						gridEditProfile.Visibility = Visibility.Visible;
						MessageBox.Show("Password changed!", "Success");
					}

					connection.Close();
				}
			}
			catch (MySqlException ex)
			{
				MessageBox.Show("Database Error " + ex.Number, "Error");
			}
		}

		private void btnUpdateCancelPassword(object sender, RoutedEventArgs e)
		{
			FillDataGrid(currentTab);
			gridEditPassword.Visibility = Visibility.Hidden;
			gridEditProfile.Visibility = Visibility.Visible;
			txtEditPassword.Clear();

		}

		private string HashPassword(string password)
		{
			return BCrypt.Net.BCrypt.HashPassword(password, 12);
		}

		private bool VerifyPassword(string password, string hashedPassword)
		{
			return BCrypt.Net.BCrypt.Verify(password, hashedPassword);
		}

		private bool EmailValidation(string email)
		{
			string pattern = @"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$";
			if (Regex.IsMatch(email, pattern))
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		// Input Validation Function that uses Regular Expressions that takes an Array of Strings
		private bool InputValidation(string[] info)
		{
			for(int i = 0; i < info.Length; i++)
			{
				if(!Regex.IsMatch(info[i].Trim(), "^[A-Za-z0-9- !@#$%^&*().,?]{1,100}$"))
				{
					return false;
				}
			}

			return true;
		}

		// Input Validation Function that takes a Single String
		private bool InputValidation(string info)
		{
			
			if (Regex.IsMatch(info.Trim(), "^[A-Za-z0-9- !@#$%^&*().,?]{1,100}$"))
			{
				return true;
			}
			else
			{
				return false;
			}
		}

	}
}
